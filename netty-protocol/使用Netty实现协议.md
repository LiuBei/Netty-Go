# 使用Netty实现协议
> Netty is an asynchronous event-driven network application framework 
for rapid development of maintainable high performance protocol servers & clients.

#### Netty是一个异步事件驱动网络应用开发框架，能快速开发出高性能、易于维护的协议服务端和客户端程序。
Maven依赖
```
  <dependency>
    <groupId>io.netty</groupId>
    <artifactId>netty-all</artifactId>
    <version>4.1.14.Final</version>
    <scope>compile</scope>
  </dependency>
```

## 使用Netty实现Discard协议
### Discard协议介绍
> It is intended for testing, debugging, measurement, or host management purposes.

[Discard协议](https://tools.ietf.org/html/rfc863)被翻译为抛弃协议，是一个有用的调试工具，通信协议里的Hello world。

### 协议实现
![netty线程模型](resources/reactor-multithread.png)
![netty线程模型2](resources/reactor-multithread2.png)

### Netty的基本用法
```
   public void run() throws Exception {
        // Boss Reactor
        EventLoopGroup bossGroup = new NioEventLoopGroup();
        // Worker Reactor
        EventLoopGroup workerGroup = new NioEventLoopGroup();
        try {
            // 服务端引导
            ServerBootstrap b = new ServerBootstrap();
            b.group(bossGroup, workerGroup)
            .channel(NioServerSocketChannel.class)
            .childHandler(new ChannelInitializer<SocketChannel>() {
                protected void initChannel(SocketChannel ch) throws Exception {
                    ch.pipeline().addLast(new DiscardServerHandler());
                }
            })
            .option(ChannelOption.SO_BACKLOG, 128)
            .childOption(ChannelOption.SO_KEEPALIVE, true);

            ChannelFuture f = b.bind(port).sync();
            f.channel().closeFuture().sync();
        } finally {
            workerGroup.shutdownGracefully();
            bossGroup.shutdownGracefully();
        }
    }
```

通道读取到数据后触发channelRead事件：
```
@Override
public void channelRead(ChannelHandlerContext ctx, Object msg) {
    ByteBuf in = (ByteBuf) msg;
    try {
        while (in.isReadable()) { 
            System.out.print((char) in.readByte());
            System.out.flush();
        }
    } finally {
        ReferenceCountUtil.release(msg);
    }
}
```

## 使用Netty实现Echo协议
### Echo协议介绍
[Echo协议](https://tools.ietf.org/html/rfc862)是回显协议，回显服务服务端将任何接收到的数据都被发送回客户端。
```
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {
        ctx.write(msg); 
        ctx.flush();
    }
```


## 使用Netty实现一个聊天室
### 聊天室需求
* 聊天室分为客户端和服务端
* 一个客户端上线通知
* 客户端离线通知
* 多客户端聊天功能

### Netty中的事件


## 使用Netty实现自定义协议

### 协议文档-Luck协议
>协议名：luck-字符串协议   
协议组成： Header | Content    
Header:     
| Version | Content-Length | SessionId | Content |  
Content:    
要传输的协议内容

### Netty提供的编码解码模块
Netty既提供了通用的编解码框架供用户扩展，又提供了常用的编解码类库供用户直接使用。
![netty逻辑图](resources/netty-logic.png)
* 解码：将二进制数据转换成应用程序协议信息或者业务信息
* 编码：将应用程序协议信息或者业务信息转换成二进制字节数组（ByteBuf）

StringDecoder解码过程介绍：
```
    protected void decode(ChannelHandlerContext ctx, ByteBuf msg, List<Object> out) throws Exception {
        out.add(msg.toString(charset));
    }
```

Netty已经提供的解码协议，例如：
http、websocket、redis、mqtt、base64


### Netty提供的拆包功能
TCP协议以流的形式进行传输数据，会出现粘包问题，为解决粘包问题，会有如下四种拆包方法：

1. 消息长度固定  
1. 回车换行符作为消息结束符
1. 特殊的分隔符作为消息结束符
1. 消息头中定义字段域长度

相关的API介绍：
* FixedLengthFrameDecoder
* LineBasedFrameDecoder
* DelimiterBasedFrameDecoder
* LengthFieldBasedFrameDecoder

#### FixedLengthFrameDecoder
FixedLengthFrameDecoder是固定长度解码器，它能够按照指定的长度对消息进行自动解码。
```
    /**
     * @param frameLength 数据包长度 
     */
    public FixedLengthFrameDecoder(int frameLength) {
        if (frameLength <= 0) {
            throw new IllegalArgumentException(
                    "frameLength must be a positive integer: " + frameLength);
        }
        this.frameLength = frameLength;
    }
```

#### LineBasedFrameDecoder
LineBasedFrameDecoder是回车换行解码器，如果用户发送的消息以回车换行符作为消息结束的标识，
则可以直接使用Netty的LineBasedFrameDecoder对消息进行解码，LineBasedFrameDecoder的工作原理是它依次遍历ByteBuf中的可读字节，
判断看是否有“\n”或者“\r\n”，如果有，就以此位置为结束位置，从可读索引到结束位置区间的字节就组成了一行。
它是以换行符为结束标志的解码器，支持携带结束符或者不携带结束符两种解码方式，同时支持配置单行的最大长度。
如果连续读取到最大长度后仍然没有发现换行符，就会抛出异常，同时忽略掉之前读到的异常码流。
防止由于数据报没有携带换行符导致接收到ByteBuf无限制积压，引起系统内存溢出。

```
    /**
     * @param maxLength 单个数据包的最大长度 
     */
    public LineBasedFrameDecoder(final int maxLength) {
        this(maxLength, true, false);
    }
```

#### DelimiterBasedFrameDecoder 
自定义的分隔符解码，常用构造方法：
```
        /**
         * @param maxFrameLength  单个数据包的最大长度
         * 当达到该长度后仍然没有查到分隔符，就抛出TooLongFrameException异常
         * 防止由于异常码流缺失分隔符导致的内存溢出
         * @param delimiter  the delimiter 分隔符
         */
    public DelimiterBasedFrameDecoder(int maxFrameLength, ByteBuf delimiter) {
        this(maxFrameLength, true, delimiter);
    }
```
（1）参数1设置最大长度  
（2）参数2可以通过如下方法获得： Delimiters.lineDelimiter()); 或者 Delimiters.nulDelimiter())  
（3）或者自定义消息结束符
```
   ByteBuf delimiter = Unpooled.copiedBuffer("$_".getBytes());
   ch.pipeline().addLast(new DelimiterBasedFrameDecoder(1024, delimiter));
```

#### LengthFieldBasedFrameDecoder
协议消息头中定义字段域长度
```
    /**
     * @param maxFrameLength 单个数据包的最大长度
     * @param lengthFieldOffset 长度域字段的起始偏移量
     * @param lengthFieldLength 长度域字段的字节长度
     * @param lengthAdjustment 长度调节值
     * @param initialBytesToStrip 解码后的数据包需要跳过的头部信息的字节数
     */
    public LengthFieldBasedFrameDecoder(
            int maxFrameLength,
            int lengthFieldOffset, int lengthFieldLength,
            int lengthAdjustment, int initialBytesToStrip) {
        this(
                maxFrameLength,
                lengthFieldOffset, lengthFieldLength, lengthAdjustment,
                initialBytesToStrip, true);
    }
```

#### 资料：
[演讲代码](http://git.oschina.net/LiuBei/Netty-Go/tree/master/netty-protocol)、
[在线文档](http://git.oschina.net/LiuBei/Netty-Go/blob/master/netty-protocol/%E4%BD%BF%E7%94%A8Netty%E5%AE%9E%E7%8E%B0%E5%8D%8F%E8%AE%AE.md)
