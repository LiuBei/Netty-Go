package com.yitianyigexiangfa.netty.protocol.oio;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class PerformanceTestClient {
    private static String host = "localhost";
    private static int port = 8081;
    static List<Socket> clientList = new ArrayList<Socket>();

    public static void main(String[] args) {
        for (int i = 0; i < 10000; i++) {
            try {
                Socket client = new Socket(host, port);
                clientList.add(client);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Scanner scan = new Scanner(System.in);
        String s = scan.nextLine();
        System.out.println(s);
    }

}