package com.yitianyigexiangfa.netty.protocol.chatroom.client;


import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ChatroomClient {

    private static String host = "localhost";
    private static int port = 8080;

    public static void main(String[] args) {
        ChatroomClient client = new ChatroomClient();
        client.run(host, port);
    }

    public void run(String host, int port) {
        NioEventLoopGroup group = new NioEventLoopGroup();
        Bootstrap b = new Bootstrap();
        b.group(group).channel(NioSocketChannel.class).handler(new ChatroomClientInitializer());

        try {
            Channel channel =     b.connect(host, port).sync().channel();
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            while (true){
                channel.writeAndFlush(reader.readLine() + "\r\n");
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            group.shutdownGracefully();
        }

    }
}
