package com.yitianyigexiangfa.netty.protocol.chatroom.server;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;

public class ChatroomServer {

    private static int port = 8080;

    public static void main(String[] args) {
        ChatroomServer chatroom = new ChatroomServer();
        chatroom.run(port);
    }

    public void run(int port) {
        NioEventLoopGroup bossGroupo = new NioEventLoopGroup(1);
        NioEventLoopGroup workerGrouop = new NioEventLoopGroup(3);
        try {
            ServerBootstrap sb = new ServerBootstrap();
            sb.group(bossGroupo, workerGrouop)
                    .channel(NioServerSocketChannel.class)
                    .childHandler(new ChatroomServerInitializer());
            ChannelFuture cf = sb.bind(port).sync();
            cf.channel().closeFuture().sync();
        } catch (InterruptedException e) {
            bossGroupo.shutdownGracefully();
            workerGrouop.shutdownGracefully();
            e.printStackTrace();
        }
    }

}
