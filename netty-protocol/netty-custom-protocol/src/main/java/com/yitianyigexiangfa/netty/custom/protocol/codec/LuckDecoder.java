package com.yitianyigexiangfa.netty.custom.protocol.codec;

import com.yitianyigexiangfa.netty.custom.protocol.bean.LuckHeader;
import com.yitianyigexiangfa.netty.custom.protocol.bean.LuckMessage;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.EmptyByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;
import java.util.UUID;

public class LuckDecoder extends ByteToMessageDecoder {

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        System.out.println("-------" + in.readableBytes());
        if (in.readableBytes() < 1){
            return;
        }

        String content = new String(in.readBytes(in.readableBytes()).array());
        out.add(content);
    }
}
