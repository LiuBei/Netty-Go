package com.yitianyigexiangfa.netty.custom.protocol.client;

import com.yitianyigexiangfa.netty.custom.protocol.codec.LuckDecoder;
import com.yitianyigexiangfa.netty.custom.protocol.codec.LuckEncoder;
import io.netty.channel.ChannelInitializer;

import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;

public class LuckClientInitializer extends ChannelInitializer<SocketChannel>{

    protected void initChannel(SocketChannel socketChannel) throws Exception {

        ChannelPipeline pipeline = socketChannel.pipeline();
        pipeline.addLast("decode", new LuckDecoder());
        pipeline.addLast("encode", new LuckEncoder());
        pipeline.addLast("handler", new LuckClientHandler());
    }
}
