package com.yitianyigexiangfa.netty.custom.protocol.client;

import com.yitianyigexiangfa.netty.custom.protocol.bean.LuckHeader;
import com.yitianyigexiangfa.netty.custom.protocol.bean.LuckMessage;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;

import java.util.UUID;

public class LuckClient {

    public static void main(String[] args) throws InterruptedException {

        NioEventLoopGroup group = new NioEventLoopGroup();
        Bootstrap bootstrap = new Bootstrap();
        try {
            bootstrap.group(group).channel(NioSocketChannel.class).handler(new LuckClientInitializer());

            Channel ch = bootstrap.connect("localhost", 8080).sync().channel();

//            ch.writeAndFlush(null);
            for (int i = 0; i < 3 ; i++) {
                int version = i;
                String content = "my custom String protocol- test";
//                System.out.println(content.length());
                String sessionId = UUID.randomUUID().toString();
                LuckHeader header = new LuckHeader(version, content.length(), sessionId);
                LuckMessage message = new LuckMessage(header, content);
                ch.writeAndFlush(message);
            }

            ch.close();
        } finally {
            group.shutdownGracefully();
        }

    }
}
