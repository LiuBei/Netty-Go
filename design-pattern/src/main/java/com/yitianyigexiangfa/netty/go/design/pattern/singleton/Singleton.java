package com.yitianyigexiangfa.netty.go.design.pattern.singleton;

public class Singleton {

    private Singleton() {
    }

    private static Singleton singleton = null;

    private static Object obj = new Object();

    public static Singleton GetInstance() {
        if (singleton == null) {
            synchronized (obj) {
                if (singleton == null){
                    new Singleton();
                }
            }
        }
        return singleton;
    }
}
