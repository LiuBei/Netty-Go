package com.yitianyigexiangfa.netty.custom.string.protocol.server;

import com.yitianyigexiangfa.netty.custom.string.protocol.codec.LuckDecoder;
import com.yitianyigexiangfa.netty.custom.string.protocol.codec.LuckEncoder;
import io.netty.channel.ChannelInitializer;

import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.string.StringEncoder;


public class LuckServerInitializer extends ChannelInitializer<SocketChannel> {

    @Override
    protected void initChannel(SocketChannel socketChannel) throws Exception {
        ChannelPipeline pipeline = socketChannel.pipeline();
//        pipeline.addLast("framer", new LengthFieldBasedFrameDecoder(10000, 4, 4, 36,0 ));
        pipeline.addLast("decoder", new LuckDecoder());
        pipeline.addLast("encoder", new LuckEncoder());
        pipeline.addLast("handler", new LuckServerHandler());
//        pipeline.addLast(new LuckServerHandler());
    }

}