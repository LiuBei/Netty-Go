package com.yitianyigexiangfa.netty.custom.string.protocol.codec;

import com.yitianyigexiangfa.netty.custom.string.protocol.bean.LuckHeader;
import com.yitianyigexiangfa.netty.custom.string.protocol.bean.LuckMessage;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

public class LuckEncoder extends MessageToByteEncoder<LuckMessage> {

    @Override
    protected void encode(ChannelHandlerContext ctx, LuckMessage message, ByteBuf out) throws Exception {
        LuckHeader header = message.getHeader();
        String content = message.getContent();
        int version = header.getVersion();
        int contentLenght = header.getContentLength();
        String sessionId = header.getSessionId();


        out.writeInt(version);
        out.writeInt(contentLenght);
        out.writeBytes(sessionId.getBytes());
        out.writeBytes(content.getBytes());
    }
}
