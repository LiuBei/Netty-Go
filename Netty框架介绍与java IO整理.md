# Netty框架介绍与java IO整理
### Netty聊天室

### BIO、NIO、AIO
#### BIO
#### NIO
#### AIO
### Netty

#### 是什么
> Netty is an asynchronous event-driven network application framework 
for rapid development of maintainable high performance protocol servers & clients.
#### Netty是一个异步事件驱动网络应用开发框架，能快速开发出高性能、易于维护的协议服务端和客户端程序。 
![Netty组件](resource/netty-components.png)
#### 设计模式

#### Netty上手
Maven依赖
```
  <dependency>
    <groupId>io.netty</groupId>
    <artifactId>netty-all</artifactId>
    <version>4.1.14.Final</version>
    <scope>compile</scope>
  </dependency>
```


#### 与协议的关系
#### 源码分析


### 使用Netty实现协议
#### 应用示例
1. DISCARD
2. ECHO
3. TELNET

#### 实现协议与应用场景
1. 粘包问题
2. 序列化








