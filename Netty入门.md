# Netty 入门
> Netty is an asynchronous event-driven network application framework 
for rapid development of maintainable high performance protocol servers & clients.

#### Netty是一个异步事件驱动网络应用开发框架，能快速开发出高性能、易于维护的协议服务端和客户端程序。 
![Netty组件](resource/netty-components.png)


## 上手
Maven依赖
```
  <dependency>
    <groupId>io.netty</groupId>
    <artifactId>netty-all</artifactId>
    <version>4.1.14.Final</version>
    <scope>compile</scope>
  </dependency>
```
![逻辑架构图](resource/logic.jpg)
#### netty源码 [官网教程](https://github.com/netty/netty/tree/4.1/example/src/main/java/io/netty/example)
hello world:Discard协议   
redis-client:使用netty实现的redis客户端程序

## 编码与解码


## Java IO
#### 阻塞  
![阻塞IO](resource/blockIO.jpg)  
#### 非阻塞   
![非阻塞IO](resource/nonBlockIO.jpg)
#### 异步
CallBack-回调
```
    public interface Fetcher {
        void fetchData(FetchCallback callback);
    }

    public interface FetchCallback {
        void onData(Data data);
        void onError(Throwable cause);
    }

    public class Worker {
        public void doWork() {
            Fetcher fetcher = null; // obtain Fetcher instance
            fetcher.fetchData(new FetchCallback() {
                @Override
                public void onData(Data data) {
                    System.out.println("Data received: " + data);
                }

                @Override
                public void onError(Throwable cause) {           
                    System.err.println("An error accour: " + cause.getMessage());
                }
            });

        }
    }

    public class Data {
        // holds your data
    }
```

## 自定义协议

### 协议文档-Luck协议
>协议名：luck-字符串协议   
协议分为 Header | Content    
Header:     
| Version | Content-Length | SessionId | Content |  
Content:    
要传输的协议内容

[自定义协议](http://git.oschina.net/LiuBei/Netty-Go/tree/master/custom-protocol)