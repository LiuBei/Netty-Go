package com.yitianyigexiangfa.netty.design.pattern.reactor.demo;

import java.io.IOException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;

public class Handler implements Runnable {

    final SocketChannel socket = null;
    final SelectionKey sk ;

    // 构造方法
    Handler(Selector sel) throws IOException{
        // 注册hanlder到Selector
        socket.configureBlocking(false);
        sk = socket.register(sel, 0);
        sk.attach(this);
        sk.interestOps(SelectionKey.OP_READ);
        sel.wakeup();
    }

    public void run() {

    }



}
