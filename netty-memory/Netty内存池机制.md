# Netty内存池机制

### Arena
Netty 将内存分为 heap（java 堆区）和 direct（直接内存）区，heap 区依靠 java GC进行内存回收，direct 区需要显式进行释放.
![Netty内存](0918_netty_memory.png)

Arena负责管理PoolChunk和PoolSubpage。
与其集中管理一整块内存，不如将其分成许多个小块来分而治之，此小块就是Arena。让我们想象一下，给几个小朋友一张大图纸，让他们随意地画点，结果可想而知，他们肯定互相顾忌而不肆意地画，从而影响画图效率，但是如果老师事先在大图上划分好每个人的区域，小朋友就可以又快又准确地在各自区域上画图了。

PoolArena 类是分区内存的基类，它有两个子类 HeapArena 和 DirectArena 分别对应 heap 区和 direct 区

### Chunk
chunk 是比 area 小一级的内存分配单元，多个 chunk 按照 usage（使用率）组成chunk list，area 包含 6 个 chunk list
```
q100 = new PoolChunkList<T>(null, 100, Integer.MAX_VALUE, chunkSize);
q075 = new PoolChunkList<T>(q100, 75, 100, chunkSize);
q050 = new PoolChunkList<T>(q050, 50, 100, chunkSize);
q025 = new PoolChunkList<T>(q050, 25, 75, chunkSize);
q000 = new PoolChunkList<T>(q025, 1, 50, chunkSize);
qInit = new PoolChunkList<T>(q000, Integer.MAX_VALUE, 25, chunkSize);
```
所有的PoolChunkList组成双向列表，qInit是列表头，随着内存的分配和释放，PoolChunk的使用率会变化，导致PoolChunk在这个双向列表里“流动”。
![PoolChunkList](0918_qInit.jpg)
- qInit:存储内存利用率0-25%的chunk
- q000:存储内存利用率1-50%的chunk
- q025:存储内存利用率25-75%的chunk
- q075:存储内存利用率75-100%的chunk
- q100:存储内存利用率100%的chunk

PoolChunk负责内存的分配与回收,PoolChun默认由2048个page组成，一个page默认大小为8k。
PoolChunk中维护了一个大小为2048的PoolSubPage数组。

### ByteBuf
ByteBuf 是 netty 对 byte[] 的封装，提供顺序和随机读写字节的功能。ByteBuf 是一个抽象类 ，它有众多的子类
功能类（是否缓存， 内存分配方式，是否使用unsafe）
- PooledHeapByteBuf
- PooledUnsafeHeapByteBuf
- PooledDirectByteBuf
- PooledUnsafeDirectByteBuf
- UnpooledHeapByteBuf
- UnpooledUnsafeHeapByteBuf
- UnpooledDirectByteBuf
- UnpooledUnsafeDirectBuf

包装类
- ReadOnlyByteBufferBuf
- CompositeByteBUf

从内存分配的角度看，ByteBuf分为两类：
1. 堆内存（HeapByteBuf）字节缓冲区，特点是JVM自动回收，内存的分配和回收速度快，缺点就是如果进行Socket的IO操作，需要额外做一次内存复制，将堆内存对应的缓冲区复制到内核Channel中，性能会有一定程度的下降。
2. 直接内存（DirectByteBuf)字节缓冲区，非堆内存，它在堆外进行内存分配，相比于堆内存，它的分配和回收速度回慢一些，但是从Socket Channel中读取时，由于少了一次内存复制，速度比堆内存快。

Netty的最佳实践是在I/O通信线程的读写缓冲区使用DirectByteBuf，后端业务消息的编解码模块使用HeapByteBuf。

ByteBuff的四种声明方式：
```
ByteBuf heapBuffer = Unpooled.buffer();  
          
ByteBuf directBuffer = Unpooled.directBuffer();  
          
ByteBuf wrappedBuffer = Unpooled.wrappedBuffer(new byte[128]);  
          
ByteBuf copiedBuffer = Unpooled.copiedBuffer(new byte[128]);  
```

